package utility_comman_method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class execl_data_extractor {

	public static ArrayList<String> Excel_data_reader(String filename,String sheet_name,String tc_name) throws IOException {
		ArrayList<String> array_data= new ArrayList<String>();
		String project_dir=System.getProperty("user.dir"); //fetch the current project directory
         System.out.println(project_dir); 
         //step 1 create the object of file input stream to locate the data file
         FileInputStream FIS= new FileInputStream(project_dir+"\\data_files\\"+filename+".xlsx");
         
         //step 2 create the Xssf work book object to open the excel file
         XSSFWorkbook wb = new XSSFWorkbook(FIS);
 		//step3 fetch the no of sheets available on excel file
 		int count = wb.getNumberOfSheets();
 		System.out.println(count);
 		//step4 access the sheet as per the input sheet name
 		for (int i=0; i<count; i++) {
 			String sheetname = wb.getSheetName(i);
 			if(sheetname.equals(sheet_name)) {
 	 			System.out.println(sheetname);
 	 			
 	 			XSSFSheet sheet = wb.getSheetAt(i);
 	 			Iterator<Row> row=sheet.iterator();
 	 			row.next();
 	 			
 	 			while (row.hasNext())
 	 			{
 	 				Row datarow= row.next();
 	 				String TC_name=datarow.getCell(0).getStringCellValue();
 	 				System.out.println(TC_name); 
 	 				
 	 				if(TC_name.equals(tc_name)) {
 	 					Iterator<Cell> cellvalues= datarow.iterator();
 	 					
 	 					
 	 					while (cellvalues.hasNext()) {
 	 						String testData= cellvalues.next().getStringCellValue();
 	 						System.out.println(testData); 
 	 						array_data.add(testData);
 	 					}
 	 				break;	
 	 				}
 	 			}

 			}
 		}
 		wb.close();
     return array_data;
	}
	
	

}
