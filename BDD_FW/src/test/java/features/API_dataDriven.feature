Feature: Trigger post API

Scenario Outline: Trigger the ApI request with valid request parameters 
          Given Enter "<name>" and "<job>" in request body
          When Send the request with data
          Then Validate Data_driven_statusCode 
          And Validate Data_Driven_responseBody parameter
          
Examples:
        |name|job|
        |uma|QA|
        |megha|SQA|
        |swati|TL|
        |nikhil|MANAGER|